/*

Copyright 2013-2013 Theo J.A. de Vries

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import qbs 1.0

SphinxModule {
  name: 'PdfModule'
  condition: true

  property stringList pdfExtensions: []
  PropertyOptions {
    name: "pdfExtensions"
    description: "Rst2pdf extension module names."
  }

  property string pdfSphinxConfContent: {
    if (!(sphinxExtensions.contains('rst2pdf.pdfbuilder'))) {
          sphinxExtensions.push('rst2pdf.pdfbuilder')
    }
    var confContent = "\n\
#  -- Options for pdf output --\n\
\n\
pdf_documents = [\n\
        ('"
    if (master_doc !== '') {
      confContent += master_doc
    } else if  (product.name !== '') {
      confContent += product.name
    } else {
      confContent += project.name
    }
    confContent += "', u'"
    if (product.name !== '') {
      confContent += product.name
    } else {
      confContent += project.name
    }
    confContent += "', u'"
    if (product.title !== '') {
      confContent += product.title
    } else {
      confContent += project.title
    }
    confContent += "', u'"
    if (product.authors !== '') {
      confContent += product.authors
    } else {
      confContent += project.authors
    }
    confContent += "'),\n\
]\n\
pdf_extensions = ["
      for (var i in pdfExtensions) {
        confContent += "'" + pdfExtensions[i] + "', ";
      }
      confContent += "]\n\
"
      return sphinxConfContent + confContent;
    }
}
