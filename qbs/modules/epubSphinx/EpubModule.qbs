/*

Copyright 2013-2013 Theo J.A. de Vries

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import qbs 1.0

SphinxModule {
  name: 'EpubModule'
  condition: true

// config properties for the epub builder:

  property string epub_basename: ''
  PropertyOptions {
    name: "epub_basename"
    description: "The basename."
  }

  property string epub_theme: ''
  PropertyOptions {
    name: "epub_theme"
    description: "The theme to use."
  }

  property string epub_title: ''
  PropertyOptions {
    name: "epub_title"
    description: "The title."
  }

  property stringList epub_author: []
  PropertyOptions {
    name: "epub_author"
    description: "The author(s)."
  }

  property string epub_publisher: ''
  PropertyOptions {
    name: "epub_publisher"
    description: "The publisher."
  }

  property string epub_copyright: ''
  PropertyOptions {
    name: "epub_copyright"
    description: "The copyright text."
  }

  property string epub_language: ''
  PropertyOptions {
    name: "epub_language"
    description: "The language of the text. It defaults to the language option or 'en' if the language is not set."
  }

  property int epub_tocdepth: 3
  PropertyOptions {
    name: "epub_tocdepth"
    description: "The depth of the table of contents in toc.ncx."
  }


  property string epubSphinxConfContent: {
    var confContent = "\n\
#  -- Options for epub output --\n\
\n"
    if (epub_basename !== '') {
      confContent += "epub_basename = '"
      confContent += epub_basename
      confContent += "'\n\
"
    }
    if (epub_theme !== '') {
      confContent += "epub_theme = '"
      confContent += epub_theme
      confContent += "'\n\
"
    }
    confContent += "\
epub_title = '"
    confContent += epub_title
    confContent += "'\n\
epub_author = '"
    for (var i in epub_author) {
      confContent += epub_author[i] + " ";
    }
    confContent += "'\n\
epub_publisher = '"
    confContent += epub_publisher
    confContent += "'\n\
epub_copyright = '"
    confContent += epub_copyright
    confContent += "'\n\
"
    if (epub_language !== '') {
      confContent += "epub_language = '"
      confContent += epub_language
      confContent += "'\n"
    }
    confContent += "\
epub_tocdepth = "
    confContent += epub_tocdepth

    return sphinxConfContent + confContent;
  }
}
