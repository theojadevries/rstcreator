/*

Copyright 2013-2017 Theo J.A. de Vries

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import qbs 1.0
import qbs.TextFile
import qbs.ModUtils

Module {
  name: 'SphinxModule'
  condition: false

  additionalProductTypes: ["sphinxConf"]  // to include all rules that generate sphinxconf files

// properties for sphinx:

  property string sphinxCall: {
    var spCall = ""
    if (product.builderTag == "pdf")
      spCall = "sphinx-build2"
    else
      spCall = "sphinx-build"
    return spCall
  }
  PropertyOptions {
    name: "sphinxCall"
    allowedValues: ['sphinx-build2', 'sphinx-build']
    description: "The sphinx version to call; sphinx-build2 for python2 (e.g. in case of rst2pdf), sphinx-build for python3."
  }


  property string pythonCall: {
    var pyCall = ""
    if (product.builderTag == "pdf")
      pyCall = "python2"
    else
      pyCall = "python"
    return pyCall
  }
  PropertyOptions {
    name: "pythonCall"
    allowedValues: ['python', 'python2', 'python3']
    description: "The python version to call."
  }

// config properties for general configuration:

  property stringList pythonStart: []
  PropertyOptions {
    name: "pythonStart"
    description: "Python code lines to be embedded at the start of conf.py. For example, if extensions (or modules to document with autodoc) are in the current directory, add it here using 'sys.path.insert(0, os.path.abspath('.'))'."
  }

  property stringList pythonEnd: []
  PropertyOptions {
    name: "pythonEnd"
    description: "Python code lines to be embedded at the end of the general configuration part of conf.py."
  }

  property stringList sphinxExtensions: []
  PropertyOptions {
    name: "sphinxExtensions"
    description: "Sphinx extension module names."
  }

  property string source_suffix: '.rst'
  PropertyOptions {
    name: "source_suffix"
    description: "The suffix of restructured text source filenames."
  }

  property string source_encoding: 'utf-8-sig'
  PropertyOptions {
    name: "source_encoding"
    description: "The encoding of restructured text source files."
  }

  property string master_doc: ''
  PropertyOptions {
    name: "master_doc"
    description: "The master toctree document."
  }

  property stringList exclude_patterns: []
  PropertyOptions {
    name: "exclude_patterns"
    description: "List of patterns, relative to source directory, that match files and directories to ignore when looking for source files."
  }

  property stringList templates_path: ['_templates']
  PropertyOptions {
    name: "templates_path"
    description: "Relative paths that contain templates"
  }

  property string template_bridge: ''
  PropertyOptions {
    name: "template_bridge"
    description: "A callable used to render HTML documents (or output of other builders)."
  }

  property string rst_epilog: ''
  PropertyOptions {
    name: "rst_epilog"
    description: "A string of reStructuredText that is included at the end of every source file; add general substitutions here."
  }

  property string rst_prolog: ''
  PropertyOptions {
    name: "rst_prolog"
    description: "A string of reStructuredText that is included at the start of every source file."
  }

  property string primary_domain: 'None'
  PropertyOptions {
    name: "primary_domain"
    description: "The name of the default domain."
  }

  property string default_role: 'None'
  PropertyOptions {
    name: "default_role"
    description: "The reST default role (used for this markup: `text`) to use for all documents."
  }

  property bool keep_warnings: false
  PropertyOptions {
    name: "keep_warnings"
    description: "If true, keep warnings as system message paragraphs in built documents."
  }

  property string needs_sphinx: '1.0'
  PropertyOptions {
    name: "needs_sphinx"
    description: "Minimal Sphinx version that is needed."
  }

  property bool nitpicky: false
  PropertyOptions {
    name: "nitpicky"
    description: "If true, Sphinx will warn about all references where the target cannot be found."
  }

  property stringList nitpick_ignore: []
  PropertyOptions {
    name: "nitpick_ignore"
    description: "A list of (type,target) tuples such as ('py:func','int') that should be ignored when generating warnings in nitpicky mode."
  }

// config properties for project information

  property string project_description: ''
  PropertyOptions {
    name: "project_description"
    description: "General information about the project."
  }

  property string copyright: ''

  property string version: ''
  PropertyOptions {
    name: "version"
    description: "The short X.Y version; acts as replacement for |version|, also used in various other places throughout the built documents."
  }

  property string release: ''
  PropertyOptions {
    name: "release"
    description: "The full version, including alpha/beta/rc tags; acts as replacement for |release|, also used in various other places throughout the built documents."
  }

  property string today: ''
  PropertyOptions {
    name: "today"
    description: "There are two options for replacing |today|: if you set today to some non-false value, then it is used, otherwise use today_fmt"
  }

  property stringList today_fmt: '%Y %B %d'
  PropertyOptions {
    name: "today_fmt"
    description: "If today is not set, today_fmt is used as the format for a Python strftime call."
  }

  property string highlight_language: 'python'
  PropertyOptions {
    name: "highlight_language"
    description: "The default language to highlight source code in."
  }

  property string pygments_style: 'sphinx'
  PropertyOptions {
    name: "pygments_style"
    description: "The name of the Pygments (syntax highlighting) style to use."
  }

  property bool add_function_parentheses: true
  PropertyOptions {
    name: "add_function_parentheses"
    description: "If true, '()' will be appended to :func: etc. cross-reference text."
  }

  property bool add_module_names: true
  PropertyOptions {
    name: "add_module_names"
    description: "If true, the current module name will be prepended to all description unit titles (such as .. function::)."
  }

  property bool show_authors: false
  PropertyOptions {
    name: "show_authors"
    description: "If true, sectionauthor and moduleauthor directives will be shown in the output. They are ignored by default."
  }

  property stringList modindex_common_prefix: []
  PropertyOptions {
    name: "modindex_common_prefix"
    description: "A list of ignored prefixes for module index sorting."
  }

// config properties for internationalization:
// INCOMPLETE

  property string language: 'en'
  PropertyOptions {
    name: "language"
    description: "The language for content autogenerated by Sphinx. Refer to documentation for a list of supported languages."
  }

// config properties for the html builder:
// INCOMPLETE

  property bool html_use_index: false
  PropertyOptions {
    name: "html_use_index"
    description: "Set to true to add an index in the epub."
  }

  property string confContentProlog: {
    var confContent = "#!/usr/bin/env "
    confContent += pythonCall
    confContent += "\n\
# -*- coding: utf-8 -*-\n\
\n\
import sys, os\n\
\
"
      for (var i in pythonStart) {
        confContent += pythonStart[i] + "\n";
      }
      return confContent;
    }

  property string sphinxConfContent: {
    var confContent = "\n\
# -- General configuration --\n\
\n\
extensions = ["
      for (var i in sphinxExtensions) {
        confContent += "'" + sphinxExtensions[i] + "', ";
      }
      confContent += "]\n\
source_suffix = '"
      confContent += source_suffix
      confContent += "'\n\
source_encoding = '"
      confContent += source_encoding
      confContent += "'\n\
master_doc = '"
      if (master_doc !== '') {
        confContent += master_doc
      } else {
        confContent += project.name
      }
      confContent += "'\n\
exclude_patterns = ["
      for (var i in exclude_patterns) {
        confContent += "'" + exclude_patterns[i] + "', ";
      }
      confContent += "]\n\
templates_path = ["
      for (var i in templates_path) {
        confContent += "'" + templates_path[i] + "', ";
      }
      confContent += "]\n"
      if (template_bridge !== '') {
        confContent += "template_bridge = '"
              confContent += template_bridge
              confContent += "'\n"
      }
      if (rst_epilog !== '') {
        confContent += "rst_epilog = '"
              confContent += rst_epilog
              confContent += "'\n"
      }
      if (rst_prolog !== '') {
        confContent += "rst_prolog = '"
              confContent += rst_prolog
              confContent += "'\n"
      }
      confContent += "primary_domain = "
      if (primary_domain == 'None')
        confContent += "None\n"
      else {
        confContent += "'"
              confContent += primary_domain
          confContent += "'\n"
      }
      confContent += "default_role = "
      if (default_role == 'None')
        confContent += "None\n"
      else {
        confContent += "'"
              confContent += default_role
          confContent += "'\n"
      }
      confContent += "keep_warnings = "
      keep_warnings ? confContent += 'True' : confContent += 'False'
      confContent += "\n\
needs_sphinx = '"
      confContent += needs_sphinx
      confContent += "'\n\
nitpicky = "
      nitpicky ? confContent += 'True' : confContent += 'False'
      confContent += "\n\
nitpick_ignore = ["
      for (var i in nitpick_ignore) {
        confContent += "'" + nitpick_ignore[i] + "', ";
      }
      confContent += "]\n\
\n\
# -- Project information --\n\
\n\
project = '"
      if (project_description !== '') {
        confContent += project_description
      } else if  (product.name !== '') {
        confContent += product.name
      } else {
        confContent += project.name
      }
      confContent += "'\n\
copyright = '"
      confContent += copyright
      confContent += "'\n\
version = '"
      confContent += version
      confContent += "'\n\
release = '"
      confContent += release
      confContent += "'\n\
today = '"
      confContent += today
      confContent += "'\n\
today_fmt = '"
      confContent += today_fmt
      confContent += "'\n\
add_function_parentheses = "
      add_function_parentheses ? confContent += 'True' : confContent += 'False'
      confContent += "\n\
add_module_names = "
      add_module_names ? confContent += 'True' : confContent += 'False'
      confContent += "\n\
show_authors = "
      show_authors ? confContent += 'True' : confContent += 'False'
      confContent += "\n\
pygments_style = '"
      confContent += pygments_style
      confContent += "'\n\
modindex_common_prefix = ["
      for (var i in modindex_common_prefix) {
        confContent += "'" + modindex_common_prefix[i] + "', ";
      }
      confContent += "]\n\
\n\
# -- Options for internationalization --\n\
\n\
language = '"
      confContent += language
      confContent += "'\n";

      return confContent;
    }

  property string confContentEpilog: {
    var confContent = "\n"
    for (var i in pythonEnd) {
      confContent += pythonEnd[i] + "\n"
    }
    return confContent;
  }

  FileTagger {
    patterns: [
      "*.rst",
      "*.rest",
    ]
    fileTags: [
      "rst"
    ]
  }

  FileTagger {
    patterns: ["*.png"]
    fileTags: [
      "figure",
      "png",
    ]
  }

  FileTagger {
    patterns: ["*.jpg"]
    fileTags: [
      "figure",
      "jpg",
    ]
  }

  FileTagger {
    patterns: ["*.svg"]
    fileTags: [
      "figure",
      "svg",
    ]
  }

  FileTagger {
    patterns: ["*.pdf"]
    fileTags: [
      "pdf"
    ]
  }

  FileTagger {
    patterns: ["*.html"]
    fileTags: [
      "html"
    ]
  }

  Rule {
    multiplex: true
    // no inputs -> just a generator
    Artifact {
      filePath: product.builderTag + "conf/conf.py"
      fileTags: "sphinxConf"
    }
    prepare: {
      var cmdConf = new JavaScriptCommand();
      cmdConf.confContent = product.moduleProperty(product.builderTag + 'Sphinx', 'confContentProlog')
         + product.moduleProperty(product.builderTag + 'Sphinx', product.builderTag + 'SphinxConfContent')
     + product.moduleProperty(product.builderTag + 'Sphinx', 'confContentEpilog');
      cmdConf.description = "generating " + output.filePath + ": " + cmdConf.confContent;
      cmdConf.highlight = "codegen";

      cmdConf.sourceCode = function() {
        print("Starting...");
        var file = new TextFile(output.filePath, TextFile.WriteOnly);
        file.truncate();
        file.write(confContent);
        file.close();
      }
      return cmdConf;
    }
  }

  Rule {
    id: sphinxbuilder
    multiplex: true
    inputs: ["rst"]
    auxiliaryInputs: ["sphinxConf"]

    Artifact {
      fileTags: ["sphinxProduct"]
    }
    prepare: {
      var sphinxCall =ModUtils.moduleProperty(product, "sphinxCall");
      var buildOptions = "-q";
      var outputArg = "-b " + product.builderTag;
      var confDir = "-c " + product.buildDirectory + "/" + product.builderTag + "conf";
      var cacheDir = "-d " + product.buildDirectory + "/cache";
      var sourceDir = product.sourceDirectory;
      var sphinxOutputDir = product.buildDirectory + "/"  + product.builderTag + "build";
      var args = [sphinxCall+" "+buildOptions+" "+outputArg+" "+confDir+" "+cacheDir+" "+sourceDir+" "+sphinxOutputDir];
      for (i in inputs.rst)
        args = args + " " + inputs.rst[i].filePath;
      var cmdSphinx;
      if (product.moduleProperty("qbs", "hostOS") == 'windows') {
        cmdSphinx = new Command('cmd.exe', ['/C'].concat(args));
      } else {
        cmdSphinx = new Command('/bin/sh', ['-c'].concat(args))
      }
      cmdSphinx.workingDirectory = product.buildDirectory;
      cmdSphinx.description = "building " + product.name;
      cmdSphinx.highlight = "codegen";
      return cmdSphinx;
    }
  }

}
