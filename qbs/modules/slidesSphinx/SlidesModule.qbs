/*

Copyright 2013-2013 Theo J.A. de Vries

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import qbs 1.0

SphinxModule {
  name: 'SlidesModule'
  condition: true

  property string slide_title: ''
  PropertyOptions {
    name: "slide_title"
    description: "Sets the title of slide project generated. This title will be used in the HTML title of the output. Default: inherit from htmlTitle"
  }

  property bool autoslides: true
  PropertyOptions {
    name: "autoslides"
    description: "When autoslides is true, Hieroglyph will generate slides from the document sections. If autoslides is set to false, only generate slides from the slide directive. Default: true. This can be overridden on a per-document basis using the slideconf directive."
  }

  property string slide_theme: 'slides'
  PropertyOptions {
    name: "slide_theme"
    description: "The theme to use when generating slides. Hieroglyph includes two themes, slides and single-level. Default: slides. This can be overridden on a per-document basis using the slideconf directive."
  }

  property int slide_levels: 3
  PropertyOptions {
    name: "slide_levels"
    description: "Number of Sphinx section levels to convert to slides; note that the document title is level 1. Heading levels greater than slide levels will simply be treated as slide content. Default: 3."
  }

  property bool slide_numbers: false
  PropertyOptions {
    name: "slide_numbers"
    description: "If set to true, slide numbers will be added to the HTML output. Default: false."
  }

  property string slide_footer
  PropertyOptions {
    name: "slide_footer"
    description: "Text that will be added to the bottom of every slide. Default: None."
  }

// Themes
  property var slide_theme_options: ({})
  PropertyOptions {
    name: "slide_theme_options"
    description: "Theme specific options as a stringList. Default: {}. See Custom CSS for more information."
  }

  property stringList slide_theme_path: []
  PropertyOptions {
    name: "slide_theme_path"
    description: "A list of paths to look for themes in. Default: []."
  }

  property bool slide_link_to_html: false
  PropertyOptions {
    name: "slide_link_to_html"
    description: "Link from slides to HTML. Default: false."
  }

  property bool slide_link_html_to_slides: false
  PropertyOptions {
    name: "slide_link_html_to_slides"
    description: "Link from HTML to slides. Default: false."
  }

  property bool slide_link_html_sections_to_slides: false
  PropertyOptions {
    name: "slide_link_html_sections_to_slides"
    description: "Link individual HTML sections to specific slides. Default: false. Note that slide_link_html_to_slides must be enabled for this to have any effect."
  }

  property string slide_relative_path: '../slides/'
  PropertyOptions {
    name: "slide_relative_path"
    description: "Relative path from HTML to slides; default: ../slides/"
  }

  property string slide_html_relative_path: '../html/'
  PropertyOptions {
    name: "slide_html_relative_path"
    description: "Relative path from slides to HTML; default: ../html/"
  }

  property string slide_html_slide_link_symbol: '§'
  PropertyOptions {
    name: "slide_html_slide_link_symbol"
    description: "Text used to link between HTML sections and slides. T his text is appended to the headings, similar to the section links in HTML output. Default: §"
  }

property string slidesSphinxConfContent: {
    if (!(sphinxExtensions.contains('hieroglyph'))) {
          sphinxExtensions.push('hieroglyph')
    }
    var confContent = "\n\
#  -- Options for slides output --\n\
\n\
slide_title = '"
    confContent += slide_title
    confContent += "'\n\
autoslides = "
    autoslides ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
slide_theme = '"
    confContent += slide_theme
    confContent += "'\n\
slide_levels = "
    confContent += slide_levels
    confContent += "\n\
slide_numbers = "
    slide_numbers ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
slide_theme_options = "
    confContent += JSON.stringify(slide_theme_options)
//slide_theme_options = {"
//    for (var k in slide_theme_options) {
//      confContent += "'" + k + "': ";
//      confContent += "'" + slide_theme_options[k] + "', ";
//    }
    confContent += "\n\
slide_theme_path = ["
    for (var i in slide_theme_path) {
      confContent += "'" + slide_theme_path[i] + "', ";
    }
    confContent += "]\n\
slide_link_to_html = "
    slide_link_to_html ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
slide_link_html_to_slides = "
    slide_link_html_to_slides ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
slide_link_html_sections_to_slides = "
    slide_link_html_sections_to_slides ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
slide_relative_path = '"
    confContent += slide_relative_path
    confContent += "'\n\
slide_html_relative_path = '"
    confContent += slide_html_relative_path
    confContent += "'\n\
"
    return sphinxConfContent + confContent;
  }
}
