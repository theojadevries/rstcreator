/*

Copyright 2013-2013 Theo J.A. de Vries

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

import qbs 1.0

SphinxModule {
  name: 'HtmlModule'
  condition: true

// config properties for the html builder:

  property string html_theme: 'default'
  PropertyOptions {
    name: "html_theme"
    description: "The theme that the HTML output should use. The default is 'default'."
  }

  property stringList html_theme_options: []
  PropertyOptions {
    name: "html_theme_options"
    description: "A list of options that influence the look and feel of the selected
      theme. These are theme-specific. An option always involves two list entries: first the key,
      then the value."
  }

  property stringList html_theme_path: []
  PropertyOptions {
    name: "html_theme_path"
    description: "A list of paths that contain custom themes, either as subdirectories or as
      zip files.  Relative paths are taken as relative to the configuration directory."
  }

  property string html_style: ''
  PropertyOptions {
    name: "html_style"
    description: "The style sheet to use for HTML pages.  A file of that name must exist either
      in Sphinx 'static/' path, or in one of the custom paths given in
      'html_static_path'. Default value is an empty string, which imlies the stylesheet given by the selected
      theme. If you only want to add or override a few things compared to the
      theme's stylesheet, use CSS '@import' to import the theme's stylesheet."
  }

  property string html_title: 'None'
  PropertyOptions {
    name: "html_title"
    description: "The 'title' for HTML documentation generated with Sphinx' own templates.
      This is appended to the <title> tag of individual pages, and used in the
      navigation bar as the 'topmost' element.  It defaults to 'None', which results in a title
      consisting of '{<project>} v{<revision>} documentation' (with the values coming from the config
      values)."
  }

  property string html_short_title:
  PropertyOptions {
    name: "html_short_title"
    description: "A shorter 'title' for the HTML docs.  This is used for links in the header
      and in the HTML Help docs.  If not given, it defaults to the value of 'html_title'."
  }

  property stringList html_context: []
  PropertyOptions {
    name: "html_context"
    description: "A dictionary of parameters to pass into the template engine's context for all
      pages. A parameter always involves two list entries: first the name,
      then the value."
  }

  property string html_logo: 'None'
  PropertyOptions {
    name: "html_logo"
    description: "If given, this must be the name of an image file (path relative to the
      configuration directory) that is the logo of the docs. It is placed
      at the top of the sidebar; its width should therefore not exceed 200 pixels.
      Default: 'None'."
  }

  property string html_favicon: 'None'
  PropertyOptions {
    name: "html_favicon"
    description: "If given, this must be the name of an image file (path relative to the
     configuration directory) that is the favicon of the docs.  Modern browsers use this
     as icon for tabs, windows and bookmarks. It should be a Windows-style icon
     file ('.ico'), which is 16x16 or 32x32 pixels large.  Default: 'None'."

  }

  property stringList html_static_path: []
  PropertyOptions {
    name: "html_static_path"
    description: "A list of paths that contain custom static files (such as style
     sheets or script files).  Relative paths are taken as relative to
     the configuration directory. They are copied to the output's
     '_static' directory after the theme's static files, so a file
     named 'default.css' will overwrite the theme's 'default.css'."
  }

  property stringList html_extra_path: []
  PropertyOptions {
    name: "html_extra_path"
    description: "A list of paths that contain extra files not directly related to
      the documentation, such as 'robots.txt' or '.htaccess'.
      Relative paths are taken as relative to the configuration
      directory. They are copied to the output directory. They will
      overwrite any existing file of the same name."
  }

  property string html_last_updated_fmt: '%b %d, %Y'
  PropertyOptions {
    name: "html_last_updated_fmt"
    description: "If this is not the empty string, a 'Last updated on:' timestamp is inserted
      at every page bottom, using the given 'strftime' format.  Default is
      '%b %d, %Y' (or a locale-dependent equivalent)."
  }

  property bool html_use_smartypants: true
  PropertyOptions {
    name: "html_use_smartypants"
    description: "If true, SmartyPants will be used to convert quotes and dashes to
      typographically correct entities. Default: true."
  }

  property string html_add_permalinks: '' //'¶'
  PropertyOptions {
    name: "html_add_permalinks"
    description: "Sphinx will add 'permalinks' for each heading and description environment as
      paragraph signs that become visible when the mouse hovers over them.
      This value determines the text for the permalink; it defaults to '¶'.
      Set it to 'None' or the empty string to disable permalinks."
  }

  property stringList html_sidebars: []
  PropertyOptions {
    name: "html_sidebars"
    description: "Custom sidebar templates, must be a dictionary that maps document names to
   template names. This does not work yet....."
  }

  property stringList html_additional_pages: []
  PropertyOptions {
    name: "epub_tocdepth"
    description: "Additional templates that should be rendered to HTML pages.
      Each entry consists of two list elements: a key and a value.
      Example: html_additional_pages = ['download', 'customdownload.html']
      This will render the template 'customdownload.html' as the page 'download.html'."
  }

  property bool html_domain_indices: true
  PropertyOptions {
    name: "html_domain_indices"
    description: "If true, generate domain-specific indices in addition to the general index.
      For e.g. the Python domain, this is the global module index. Default is true."
  }

  property bool html_use_index: true
  PropertyOptions {
    name: "html_use_index"
    description: "If true, add an index to the HTML documents.  Default is true."
  }

  property bool html_split_index: false
  PropertyOptions {
    name: "html_split_index"
    description: "If true, the index is generated twice: once as a single page with all the
   entries, and once as one page per starting letter. Default is false."
  }

  property bool html_copy_source: true
  PropertyOptions {
    name: "html_copy_source"
    description: "If true, the reST sources are included in the HTML build as
   '_sources/{name}'. The default is true."
  }

  property bool html_show_sourcelink: true
  PropertyOptions {
    name: "html_show_sourcelink"
    description: "If true (and 'html_copy_source' is true as well), links to the
   reST sources will be added to the sidebar. The default is true."
  }

  property string html_use_opensearch: ''
  PropertyOptions {
    name: "html_use_opensearch"
    description: "If nonempty, an 'OpenSearch <http://opensearch.org>' description file will be
      output, and all pages will contain a '<link>' tag referring to it.  Since
      OpenSearch doesn't support relative URLs for its search page location, the
      value of this option must be the base URL from which these documents are
      served (without trailing slash), e.g. 'http://docs.python.org'. The
      default is the empty string."
  }

  property string html_file_suffix: '.html'
  PropertyOptions {
    name: "html_file_suffix"
    description: "This is the file name suffix for generated HTML files.  The default is
   '.html'."
  }

  property string html_link_suffix: 'None'
  PropertyOptions {
    name: "html_link_suffix"
    description: "Suffix for generated links to HTML files.  The default is whatever
   'html_file_suffix' is set to; it can be set differently (e.g. to
   support different web server setups)."
  }

  property string html_translator_class: 'None'
  PropertyOptions {
    name: "html_translator_class"
    description: "A string with the fully-qualified name of a HTML Translator class, that is, a
    subclass of Sphinx' sphinx.writers.html.HTMLTranslator, that is used
    to translate document trees to HTML.  Default is 'None' (use the builtin
    translator)."
  }

  property bool html_show_copyright: true
  PropertyOptions {
    name: "html_show_copyright"
    description: "If true, '(C) Copyright ...' is shown in the HTML footer. Default is true."
  }

  property bool html_show_sphinx: true
  PropertyOptions {
    name: "html_show_sphinx"
    description: "If true, 'Created using Sphinx' is shown in the HTML footer.  Default is
    true."
  }

  property string html_output_encoding: 'utf-8'
  PropertyOptions {
    name: "html_output_encoding"
    description: "Encoding of HTML output files. Default is 'utf-8'.  Note that this
    encoding name must both be a valid Python encoding name and a valid HTML
    'charset' value."
  }

  property bool html_compact_lists: true
  PropertyOptions {
    name: "html_compact_lists"
    description: "If true, list items containing only a single paragraph will not be rendered
    with a '<p>' element.  This is standard docutils behavior.  Default: true."
  }

  property string html_secnumber_suffix: '. '
  PropertyOptions {
    name: "html_secnumber_suffix"
    description: "Suffix for section numbers. Default: '. '.  Set to ' ' to suppress
    the final dot on section numbers."
  }

  property string html_search_language: 'None'
  PropertyOptions {
    name: "html_search_language"
    description: "Language to be used for generating the HTML full-text search index. This
    defaults to the global language selected with :confval:`language`.  If there
    is no support for this language, ``en`` is used which selects the English
    language.

    Support is present for these languages:

    * ``da`` -- Danish
    * ``nl`` -- Dutch
    * ``en`` -- English
    * ``fi`` -- Finnish
    * ``fr`` -- French
    * ``de`` -- German
    * ``hu`` -- Hungarian
    * ``it`` -- Italian
    * ``ja`` -- Japanese
    * ``no`` -- Norwegian
    * ``pr`` -- Portuguese
    * ``ro`` -- Romanian
    * ``ru`` -- Russian
    * ``es`` -- Spanish
    * ``sv`` -- Swedish
    * ``tr`` -- Turkish

    .. admonition:: Accelerate build speed

      Each language (except Japanese) provides its own stemming algorithm.
      Sphinx uses a Python implementation by default.  You can use a C
      implementation to accelerate building the index file.

      * `PorterStemmer <https://pypi.python.org/pypi/PorterStemmer>`_ (``en``)
      * `PyStemmer <https://pypi.python.org/pypi/PyStemmer>`_ (all languages)"
  }

  property stringList html_search_options: []
  PropertyOptions {
    name: "html_search_options"
    description: "A dictionary with options for the search language support, empty by default.
    The meaning of these options depends on the language selected.

    The English support has no options.

    The Japanese support has these options:

    * ``type`` -- ``'mecab'`` or ``'default'`` (selects either MeCab or
      TinySegmenter word splitter algorithm)
    * ``dic_enc`` -- the encoding for the MeCab algorithm
    * ``dict`` -- the dictionary to use for the MeCab algorithm
    * ``lib`` -- the library name for finding the MeCab library via ctypes if the
      Python binding is not installed"
  }

  property string html_search_scorer: ''
  PropertyOptions {
    name: "html_search_scorer"
    description: "The name of a javascript file (relative to the configuration directory) that
    implements a search results scorer.  If empty, the default will be used."
  }

  property string htmlhelp_basename: 'pydoc'
  PropertyOptions {
    name: "htmlhelp_basename"
    description: "Output file base name for HTML help builder.  Default is 'pydoc'."
  }

  property string htmlSphinxConfContent: {
    var confContent = "\n\
#  -- Options for html output --\n"
    confContent += "\n\
html_theme = '"
    confContent += html_theme;
    confContent += "'\n\
"
    if (html_theme_options.length > 0) {
      confContent += "html_theme_options = ["
      for (var i in html_theme_options) {
        confContent += "'" + html_theme_options[i];
        if (i % 2 == 0) {
          confContent += "': ";
        } else {
          confContent += "', ";
        }
      }
      confContent += "]\n\
"
    }
    if (html_theme_path.length > 0) {
      confContent += "html_theme_path = ["
      for (var i in html_theme_path) {
        confContent += "'" + html_theme_options[i] + "', ";
      }
      confContent += "]\n\
"
    }
    confContent += "html_style = '"
    confContent += html_style;
    confContent += "'\n\
html_title = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_title;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
html_short_title = '"
    confContent += html_short_title;
    confContent += "'\n\
"
    if (html_context.length > 0) {
      confContent += "html_context = ["
      for (var i in html_context) {
        confContent += "'" + html_context[i];
        if (i % 2 == 0) {
          confContent += "': ";
        } else {
          confContent += "', ";
        }
      }
      confContent += "]\n\
"
    }
    confContent += "html_logo = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_logo;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
html_favicon = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_favicon;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
"
    if (html_static_path.length > 0) {
      confContent += "html_static_path = ["
      for (var i in html_static_path) {
        confContent += "'" + html_static_path[i] + "', ";
      }
      confContent += "]\n\
"
    }
    if (html_extra_path.length > 0) {
      confContent += "html_extra_path = ["
      for (var i in html_extra_path) {
        confContent += "'" + html_extra_path[i] + "', ";
      }
      confContent += "]\n\
"
    }
    confContent += "html_last_updated_fmt = '"
    confContent += html_last_updated_fmt;
    confContent += "'\n\
html_use_smartypants = "
    html_use_smartypants ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_add_permalinks = '"
    confContent += html_add_permalinks;
    confContent += "'\n\
"
    if (html_sidebars.length > 0) {
      confContent += "html_sidebars = ["
      for (var i in html_sidebars) {
        confContent += "'" + html_sidebars[i] + "', ";
      }
      confContent += "]\n\
"
    }
    if (html_additional_pages.length > 0) {
      confContent += "html_additional_pages = ["
      for (var i in html_additional_pages) {
        confContent += "'" + html_additional_pages[i] + "', ";
      }
      confContent += "]\n\
"
    }
    confContent += "html_domain_indices = "
    html_domain_indices ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_use_index = "
    html_use_index ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_split_index = "
    html_split_index ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_copy_source = "
    html_copy_source ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_show_sourcelink = "
    html_show_sourcelink ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_use_opensearch = '"
    confContent += html_use_opensearch;
    confContent += "'\n\
html_file_suffix = '"
    confContent += html_file_suffix;
    confContent += "'\n\
html_link_suffix = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_link_suffix;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
html_translator_class = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_translator_class;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
html_show_copyright = "
    html_show_copyright ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_show_sphinx = "
    html_show_sphinx ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_output_encoding = '"
    confContent += html_output_encoding;
    confContent += "'\n\
html_compact_lists = "
    html_compact_lists ? confContent += 'True' : confContent += 'False'
    confContent += "\n\
html_secnumber_suffix = '"
    confContent += html_secnumber_suffix;
    confContent += "'\n\
html_search_language = "
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += html_search_language;
    if (html_translator_class != 'None')
      confContent += "'"
    confContent += "\n\
"
    if (html_search_options.length > 0) {
      confContent += "html_search_options = ["
      for (var i in html_search_options) {
        confContent += "'" + html_search_options[i] + "', ";
      }
      confContent += "]\n\
"
    }
    confContent += "html_search_scorer = '"
    confContent += html_search_scorer;
    confContent += "'\n\
htmlhelp_basename = '"
    confContent += htmlhelp_basename;
    confContent += "'\n\n"

    return sphinxConfContent + confContent;
  }
}

