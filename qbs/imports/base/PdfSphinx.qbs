import qbs 1.0

Product {
  property string builderTag: "pdf"
  property string productFileExtension: "pdf"

  property string name: ""
  property string title: ""
  property stringList authors: []

  type: "sphinxProduct"
  Depends { name: 'pdfSphinx' }
}

