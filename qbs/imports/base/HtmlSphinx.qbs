import qbs 1.0

Product {
  property string builderTag: "html"
  property string productFileExtension: "html"

  property string name: ""
  property string title: ""
  property stringList authors: []

  type: "sphinxProduct"
  Depends { name: 'htmlSphinx' }
}

