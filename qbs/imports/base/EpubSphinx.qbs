import qbs 1.0

Product {
  property string builderTag: "epub"
  property string productFileExtension: "epub"

  property string name: ""
  property string title: ""
  property stringList authors: []

  type: "sphinxProduct"
  Depends { name: 'epubSphinx' }
}

