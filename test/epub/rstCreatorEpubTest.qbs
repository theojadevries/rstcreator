import qbs 1.0

Project {
  property string name: "rstCreatorTest"

  EpubSphinx {
      name: project.name + '-' + builderTag
      title: "The Title"
      authors: ["Theo J.A. de Vries", ]

      files: [
          "rstCreatorTest.rst",
      ]

      epubSphinx.master_doc: project.name
  }
}
