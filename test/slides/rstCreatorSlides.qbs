import qbs 1.0

Project {
  name: "rstCreatorSlides"

  SlidesSphinx {
    title: "The Title"
    authors: [
      "Theo J.A. de Vries",
    ]

    files: [
      "rstCreatorSlides.rst",
    ]

    property int testtt: 1.1
    slidesSphinx.pythonStart: ["slide_numbers = True", ]
    slidesSphinx.sphinxExtensions: ['sphinx.ext.mathjax', ]

  }

}
