import qbs 1.0

Project {
  property string name: "rstCreatorTest"
  property string title: "The Title"
  property stringList authors: ["Theo J.A. de Vries", ]

  property stringList commonFiles: [
          "rstCreatorTest.rst",
      ]

  PdfSphinx {
      name: project.name + '-' + builderTag
      title: project.title
      authors: project.authors

      files: project.commonFiles

      pdfSphinx.master_doc: project.name
      pdfSphinx.show_authors: true
  }

  HtmlSphinx {
      name: project.name + '-' + builderTag
      title: project.title
      authors: project.authors

      files: project.commonFiles

      htmlSphinx.master_doc: project.name
  }

  EpubSphinx {
      name: project.name + '-' + builderTag
      title: project.title
      authors: project.authors

      files: project.commonFiles

      epubSphinx.master_doc: project.name
  }
}
