import qbs 1.0
import sphinx

Project {
  property string name: "rstCreatorTest"
  property string title: ""
  property string authors: ["Theo J.A. de Vries", ]

  qbsSearchPaths: '../../qbs'

Product {
  type: "sphinxProduct"

  property string builderTag: "epub"
  property string name: project.name + "." + builderTag
  property string title: ""
  property string authors: ""

  Depends { name: 'sphinxx' }
}

Product {
  type: "sphinxProduct"

  property string builderTag: "pdf"
  property string name: project.name + "." + builderTag
  property string title: ""
  property string authors: ""

  Depends { name: 'sphinxx' }
}

/*  SphinxProduct {
    builderTag: "pdf"
    name: project.name + "." + builderTag
    authors: project.authors

    files: [
      "rstCreatorTest.rst",
    ]
  }


  SphinxProduct {
    builderTag: "epub"
    name: project.name + "." + builderTag
    authors: project.authors

    files: [
      "rstCreatorTest.rst",
    ]
  }
*/
}
