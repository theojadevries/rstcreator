
===========
 rstCreator
===========

What
====

rstCreator facilitates the production of various types of documents (pdf reports,
ebooks, slides, html pages, etc.) with the help of QtCreator and Sphinx.

Why?
====

* ReStructuredText, which has been described as **plain text WYSIWYG**, is
  a great format to create content with, be it on your own or in collaboration;
* Sphinx can turn rst content into a nicely styled document of a type that is
  suited for the purpose;
* QtCreator combines a powerful plain text editor (for content creation) with a
  convenient build environment (based on qbs, for processing with Sphinx)
  and project management and version control tools.

License
=======

**rstCreator** is made available under an Apache license, version 2.0;
see LICENSE-2.0 for details.

More Information
================

* `ReStructured Text`_
* `Sphinx`_
* `Docutils`_
* `Qt-Creator`_
* `Qbs`_

.. _`Sphinx`: http://sphinx.pocoo.org/
.. _`Docutils`: http://docutils.sourceforge.net/
.. _`ReStructured Text`: http://docutils.sourceforge.net/rst.html
.. _`Qt-Creator`: http://qt-project.org/doc/qtcreator/
.. _`Qbs`: http://doc-snapshot.qt-project.org/qbs/
