import qbs 1.0

Project {
  property string name: "rstCreatorTest"

  PdfSphinx {
      name: project.name + '-' + builderTag
      title: "The Title"
      authors: ["Theo J.A. de Vries", ]

      files: [
          "rstCreatorTest.rst",
      ]

      pdfSphinx.master_doc: project.name
      pdfSphinx.show_authors: true
  }
}
