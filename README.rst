============
 rSTCreator
============

**rSTCreator** enables you to build documents (epub, pdf, slides, html) from
reStructuredText. It is based on QtCreator, QBS and Sphinx. Its aim is to make your writing
efforts more productive and enjoyable.

Why this combination?
=====================

Well:

* reStructuredText is an easy-to-read, what-you-see-is-what-you-get plain text
  markup syntax and parser system that is powerful enough for non-trivial use.
  The intended purpose of the markup is the conversion of reStructuredText
  documents into useful structured data formats.
* Qt-Creator is an Integrated Documentation Environment; it has a convenient and
  productive plain text editor and tools for project management, build process
  management, version control and help.
* QBS is a newly developed build system that integrates well with Qt-Creator, is
  fast and has a pleasant syntax.
* Sphinx translates a set of reStructuredText source files into various output
  formats, automatically producing cross-references, indices etc.


Getting it up and running
=========================

Prerequisites
-------------

You need an installation of `Sphinx`_, which in turn requires `python`_.
And you need to install `Qt-Creator`_.

Installing
----------

We have no real installation process (yet).

Running
-------

#. Start QtCreator
#. Use the project wizard: File -> New File or Project -> Documents - Qbs-built reStructuredText project -> Choose...
   and complete the forms
#. Edit / add reStructuredText file(s)
#. Build
#. You find the generated document(s) in the build directory

Contributing
============

Submit wishes, comments, patches, etc. via bitbucket:
https://bitbucket.org/theojadevries/rstcreator.

License
=======

**rSTCreator** is made available under an Apache license, version 2.0;
see LICENSE-2.0 for details.

More Information
================

* `ReStructured Text`_
* `Sphinx`_
* `Docutils`_
* `Qt-Creator`_
* `Qbs`_

.. _`Sphinx`: http://sphinx.pocoo.org/
.. _`Docutils`: http://docutils.sourceforge.net/
.. _`ReStructured Text`: http://docutils.sourceforge.net/rst.html
.. _`Qt-Creator`: http://qt-project.org/doc/qtcreator/
.. _`Qbs`: http://doc-snapshot.qt-project.org/qbs/
.. _`python`: http://www.python.org/


