=======================
 rSTCreator ToDo items
=======================

High priority
=============

* install product into source dir
* QtHelp document type
* integrated help documentation
* QBS WARNING: Property fileName is deprecated. Please use filePath instead.
* Windows installer

Medium priority
===============

* Run opens document in default viewer of the OS
* Spell checker
* Better deal with multiple authors in wizard
* Examples
* Automatic tests

Low priority
============

* integrated viewer(s) in QtCreator?

