import qbs 1.0

Project {

  %BUILDERTAG:c%Sphinx {
    name: "%ProjectName%"
    title: "%TITLE%"
    authors: [
      "%AUTHOR%", 
    ]

    files: [
      "%ProjectName%.rst",
    ]

    %BUILDERTAG%Sphinx.sphinxExtensions: [
    ]
  }

}
